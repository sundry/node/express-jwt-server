const Sequelize = require('sequelize');
const env = process.env.NODE_ENV;
const config = require(__dirname + '/../config/config.js')[env];

let sequelize;
sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  config
);

const Users = require('./Users');
const Accounts = require('./Accounts');

const models = {
  Users: Users.init(sequelize, Sequelize),
  Accounts: Accounts.init(sequelize, Sequelize),
};

Object.values(models)
  .filter(model => typeof model.associate === 'function')
  .forEach(model => model.associate(models));

const db = {
  ...models,
  sequelize,
  Sequelize,
};

module.exports = db;
