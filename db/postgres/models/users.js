const Sequelize = require('sequelize');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const moment = require('moment');

class Users extends Sequelize.Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER,
        },
        accountId: {
          allowNull: false,
          type: DataTypes.INTEGER,
        },
        email: {
          allowNull: false,
          type: DataTypes.STRING,
          validate: {
            isEmail: true,
          },
          unique: true,
        },
        password: {
          allowNull: false,
          type: DataTypes.STRING,
        },
        verified: {
          allowNull: false,
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        verificationHash: {
          allowNull: true,
          type: DataTypes.STRING,
        },
        resetPasswordHash: {
          allowNull: true,
          type: DataTypes.STRING,
        },
        resetPasswordTimeout: {
          allowNull: true,
          type: DataTypes.DATE,
        },
        createdAt: {
          allowNull: false,
          type: DataTypes.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: DataTypes.DATE,
        },
        deletedAt: {
          allowNull: true,
          type: DataTypes.DATE,
        },
      },
      {
        tableName: 'users',
        sequelize,
        paranoid: true,
        hooks: {
          beforeUpdate(user) {
            if (user.changed('password')) {
              user.hashPassword();
            }
          },
          beforeCreate(user) {
            user.hashPassword();
            const today = new Date();
            const nextweek = new Date(
              today.getFullYear(),
              today.getMonth(),
              today.getDate() + 7
            );
            const str = this.email + nextweek.toISOString();
            user.verificationHash = crypto
              .createHash('md5')
              .update(str)
              .digest('hex');
          },
        },
        scopes: {
          update: {
            fields: ['password'],
          },
          create: {
            fields: ['email', 'password'],
          },
          read: {
            attributes: {
              exclude: [
                'password',
                'verified',
                'verificationHash',
                'resetPasswordHash',
                'resetPasswordTimeout',
              ],
            },
          },
        },
      }
    );
  }

  static associate(models) {
    Users.belongsTo(models.Accounts, {
      foreignKey: 'accountId',
      as: 'account',
    });
  }

  hashPassword() {
    this.password = bcrypt.hashSync(this.password);
  }
  setResetPassword(timeout) {
    const today = new Date();
    const twoweeks = new Date(
      today.getFullYear(),
      today.getMonth(),
      today.getDate() + 14
    );
    const str = this.email + twoweeks.toISOString();
    this.resetPasswordHash = crypto
      .createHash('md5')
      .update(str)
      .digest('hex');
    this.resetPasswordTimeout = moment()
      .add(Number(timeout), 'minutes')
      .format();
  }

  unsetResetPassword() {
    this.resetPasswordHash = null;
    this.resetPasswordTimeout = null;
  }

  hasValidResetPassword() {
    return moment().isBefore(this.resetPasswordTimeout);
  }

  verifyPassword(password) {
    return bcrypt.compareSync(password, this.password);
  }
}

module.exports = Users;
