const Sequelize = require('sequelize');

class Accounts extends Sequelize.Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER,
        },
        name: {
          allowNull: true,
          type: DataTypes.STRING,
        },
        address: {
          allowNull: true,
          type: DataTypes.STRING,
        },
        city: {
          allowNull: true,
          type: DataTypes.STRING,
        },
        state: {
          allowNull: true,
          type: DataTypes.STRING,
        },
        country: {
          allowNull: true,
          type: DataTypes.STRING,
        },
        phone: {
          allowNull: true,
          type: DataTypes.STRING,
        },
        timezone: {
          allowNull: true,
          type: DataTypes.STRING,
        },
        createdAt: {
          allowNull: false,
          type: DataTypes.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: DataTypes.DATE,
        },
        deletedAt: {
          allowNull: true,
          type: DataTypes.DATE,
        },
      },
      {
        paranoid: true,
        sequelize,
        tableName: 'accounts',
        scopes: {
          update: {
            fields: [
              'name',
              'address',
              'city',
              'state',
              'country',
              'phone',
              'timezone',
            ],
          },
        },
      }
    );
  }
  static associate(models) {
    Accounts.hasMany(models.Users, {
      foreignKey: 'accountId',
    });
  }
}

module.exports = Accounts;
