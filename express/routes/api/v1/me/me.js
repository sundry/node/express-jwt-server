const express = require('express');
const loggedInUser = require('../../../../middleware/loggedInUser');

const router = express.Router();

const getMe = (req, res) => {
  res.json(req.user);
};

router.get('/', loggedInUser, getMe);

module.exports = router;
