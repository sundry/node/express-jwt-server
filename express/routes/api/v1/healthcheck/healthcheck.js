const express = require('express');

const router = express.Router();

const getHealthcheck = (req, res) => {
  res.end();
};

const getError = (req, res, next) => {
  const err = new Error('test');
  next(err);
};

router.get('/', getHealthcheck);
router.get('/error', getError);

module.exports = router;
