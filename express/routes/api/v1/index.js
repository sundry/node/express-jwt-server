const express = require('express');
const healthcheck = require('./healthcheck');
const info = require('./info');
const login = require('./login');
const logout = require('./logout');
const me = require('./me');
const register = require('./register');
const accounts = require('./accounts');
const users = require('./users');
const resetPassword = require('./reset-password');

const router = express.Router();

router.use('/healthcheck', healthcheck);
router.use('/info', info);
router.use('/login', login);
router.use('/logout', logout);
router.use('/me', me);
router.use('/register', register);
router.use('/accounts', accounts);
router.use('/users', users);
router.use('/reset-password', resetPassword);

module.exports = router;
