const { expect } = require('chai');
const axios = require('axios');
const server = require('../../../../../../test/server');

let resetPasswordHash;

describe('routes', () => {
  beforeEach(async () => {
    await server.start();
  });
  afterEach(async () => {
    await server.stop();
  });
  describe('api/v1/reset-password', () => {
    it('should reset password', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/reset-password`,
        method: 'PUT',
        data: {
          email: 'developer@test.com',
          password: 'abc123',
        },
      };
      const res = await axios(options);
      expect(res.status).to.equal(200);
      expect(res.data).to.have.property('resetPasswordHash');
      resetPasswordHash = res.data.resetPasswordHash;
    });
    it('should verify reset hash', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/reset-password/verify/${resetPasswordHash}`,
        method: 'GET',
      };
      const res = await axios(options);
      expect(res.status).to.equal(200);
    });
    it('should error on bad email reset password', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/reset-password`,
        method: 'PUT',
        data: {
          email: 'test@test.test',
        },
        validateStatus: false,
      };
      const res = await axios(options);
      expect(res.status).to.equal(404);
    });
    it('should error on bad verify reset hash', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/reset-password/verify/test`,
        method: 'GET',
        validateStatus: false,
      };
      const res = await axios(options);
      expect(res.status).to.equal(404);
    });
    it('should reset password with timeout', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/reset-password`,
        method: 'PUT',
        data: {
          timeout: -1,
          email: 'developer@test.com',
          password: 'abc123',
        },
      };
      const res = await axios(options);
      expect(res.status).to.equal(200);
      expect(res.data).to.have.property('resetPasswordHash');
      resetPasswordHash = res.data.resetPasswordHash;
    });
    it('should error on timeout', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/reset-password/verify/${resetPasswordHash}`,
        method: 'GET',
        validateStatus: false,
      };
      const res = await axios(options);
      expect(res.status).to.equal(400);
    });
  });
});
