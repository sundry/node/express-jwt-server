const express = require('express');

const router = express.Router();

const resetPassword = async (req, res, next) => {
  try {
    const { Users } = req.app.get('db');
    const email = req.app.get('email');
    const user = await Users.findOne({
      where: {
        email: req.body.email,
      },
    });
    if (!user) {
      const err = new Error('User not found');
      err.status = 404;
      throw err;
    }
    user.setResetPassword(req.body.timeout || 15);
    await user.save();
    email.resetPassword(user.email, user.resetPasswordHash);
    res.json({
      resetPasswordHash: user.resetPasswordHash,
    });
  } catch (e) {
    next(e);
  }
};

const verifyHash = async (req, res, next) => {
  try {
    const { Users } = req.app.get('db');
    const user = await Users.findOne({
      where: {
        resetPasswordHash: req.params.hash,
      },
    });
    if (!user) {
      const err = new Error('User not found');
      err.status = 404;
      throw err;
    }
    if (!user.hasValidResetPassword()) {
      const err = new Error('Verification has timed out');
      err.status = 400;
      throw err;
    }
    user.unsetResetPassword();
    user.save();
    res.end();
  } catch (e) {
    return next(e);
  }
};

router.put('/', resetPassword);
router.get('/verify/:hash', verifyHash);

module.exports = router;
