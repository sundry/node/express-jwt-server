const express = require('express');
const loggedInUser = require('../../../../middleware/loggedInUser');

const router = express.Router();

const updateAccount = async (req, res, next) => {
  try {
    if (req.user.accountId !== Number(req.params.id)) {
      const err = new Error('Forbidden');
      err.status = 403;
      throw err;
    }
    const { Accounts } = req.app.get('db');
    await Accounts.scope('update').update(req.body, {
      where: {
        id: req.params.id,
      },
    });
    res.end();
  } catch (e) {
    next(e);
  }
};

router.put('/:id', loggedInUser, updateAccount);

module.exports = router;
