const express = require('express');

const router = express.Router();

const register = async (req, res, next) => {
  const { Users, Accounts } = req.app.get('db');
  const email = req.app.get('email');
  try {
    if (!req.body.email) {
      const err = new Error('email is required');
      err.status = 400;
      throw err;
    }
    if (!req.body.password) {
      const err = new Error('password is required');
      err.status = 400;
      throw err;
    }
    const existingUser = await Users.findOne({
      where: {
        email: req.body.email,
      },
    });
    if (existingUser) {
      const err = new Error(`${req.body.email} already exists`);
      err.status = 400;
      throw err;
    }
    const account = await Accounts.create();
    const data = { accountId: account.id, ...req.body };
    const user = await Users.scope('create').create(data);
    email.register(user.email, user.verificationHash);
    res.json({ verificationHash: user.verificationHash });
  } catch (e) {
    next(e);
  }
};

const completeRegistration = async (req, res, next) => {
  const { Users } = req.app.get('db');
  const { verificationHash } = req.params;
  try {
    const user = await Users.findOne({
      where: {
        verificationHash,
      },
    });
    if (!user) {
      const err = new Error(`${verificationHash} not found`);
      err.status = 404;
      throw err;
    }
    await user.update({
      verified: true,
      verificationHash: null,
    });
    const token = req.refreshJWT(user.id);
    res.json({ token });
  } catch (e) {
    next(e);
  }
};

router.post('/', register);
router.get('/complete/:verificationHash', completeRegistration);

module.exports = router;
