const { expect } = require('chai');
const axios = require('axios');
const server = require('../../../../../../test/server');

describe('routes', () => {
  beforeEach(async () => {
    await server.start();
  });
  afterEach(async () => {
    await server.stop();
  });
  describe('api/v1/logout', () => {
    it('should logout', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/logout`,
        method: 'POST',
      };
      const res = await axios(options);
      expect(res.status).to.equal(200);
    });
  });
});
