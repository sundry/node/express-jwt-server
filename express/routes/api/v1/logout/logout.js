const express = require('express');
const router = express.Router();

const logout = (req, res) => {
  res.set('JWT_TOKEN', '');
  res.clearCookie('token');
  res.end();
};

router.post('/', logout);

module.exports = router;
