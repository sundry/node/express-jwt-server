const express = require('express');

const router = express.Router();

const getInfo = (req, res) => {
  res.json({
    env: process.env.NODE_ENV,
    hostname: require('os').hostname(),
  });
};

router.get('/', getInfo);

module.exports = router;
