const { expect } = require('chai');
const axios = require('axios');
const server = require('../../../../../../test/server');
const loginUser = require('../../../../../../test/loginUser');

describe('routes', () => {
  beforeEach(async () => {
    await server.start();
  });
  afterEach(async () => {
    await server.stop();
  });
  describe('api/v1/users', () => {
    it('should error by trying to update another user', async () => {
      const user = await loginUser();
      expect(user.status).to.equal(200);
      expect(user.data).to.have.property('token');
      const token = user.data.token;
      const options = {
        url: `http://localhost:3000/api/v1/users/2?token=${token}`,
        method: 'PUT',
        data: {
          id: 2,
          password: 'abc124',
        },
        validateStatus: false,
      };
      const res = await axios(options);
      expect(res.status).to.equal(403);
      expect(res.data).to.have.property('error');
      expect(res.data.error).to.have.property('message');
      expect(res.data.error.message).to.equal('Forbidden');
    });
    it('should update user', async () => {
      const user = await loginUser();
      expect(user.status).to.equal(200);
      expect(user.data).to.have.property('token');
      const token = user.data.token;
      const options = {
        url: `http://localhost:3000/api/v1/users/1?token=${token}`,
        method: 'PUT',
        data: {
          id: 2,
          password: 'abc124',
        },
      };
      const res = await axios(options);
      expect(res.status).to.equal(200);
    });
  });
});
