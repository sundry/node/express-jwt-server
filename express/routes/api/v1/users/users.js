const express = require('express');
const loggedInUser = require('../../../../middleware/loggedInUser');

const router = express.Router();

const updateUser = async (req, res, next) => {
  try {
    if (req.user.id !== Number(req.params.id)) {
      const err = new Error('Forbidden');
      err.status = 403;
      throw err;
    }
    const { Users } = req.app.get('db');
    await Users.scope('update').update(req.body, {
      where: {
        id: req.params.id,
      },
      individualHooks: true,
    });
    res.end();
  } catch (e) {
    next(e);
  }
};

router.put('/:id', loggedInUser, updateUser);

module.exports = router;
