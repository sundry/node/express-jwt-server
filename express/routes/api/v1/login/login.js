const express = require('express');

const router = express.Router();

const login = async (req, res, next) => {
  const { Users } = req.app.get('db');
  try {
    const user = await Users.findOne({
      where: {
        email: req.body.email,
      },
    });
    if (!user || !user.verifyPassword(req.body.password)) {
      const err = new Error('Email or Password is incorrect');
      err.status = 403;
      throw err;
    }
    const token = req.refreshJWT(user.id);
    res.json({
      token,
    });
  } catch (e) {
    next(e);
  }
};

router.post('/', login);

module.exports = router;
