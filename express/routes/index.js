const express = require('express');
const ApiRoutes = require('./api');

const router = express.Router();

router.use('/api', ApiRoutes);

module.exports = router;
