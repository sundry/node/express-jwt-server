module.exports = async (req, res, next) => {
  const { Users, Accounts } = req.app.get('db');
  req.user = await Users.scope('read').findByPk(req.user.id, {
    include: [
      {
        model: Accounts,
        as: 'account',
      },
    ],
  });
  next();
};
