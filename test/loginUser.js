const axios = require('axios');

module.exports = async (email = 'developer@test.com', password = 'abc123') => {
  const options = {
    url: `http://localhost:3000/api/v1/login`,
    method: 'POST',
    data: {
      email,
      password,
    },
    validateStatus: false,
  };
  return axios(options);
};
